package org.eclipse.classes;

import java.util.Arrays;
import java.util.Date;

import org.eclipse.models.Carre;
import org.eclipse.models.Employe;
import org.eclipse.models.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println("-------------------");
        User u = new User(1,"Jean","Baton","JB@gmel");
        Gson gson = new Gson();
        String jsonString = gson.toJson(u);
        System.out.println(jsonString);
        
        System.out.println("-------------------");
        String jsonString2 = "{'id':1, 'firstname' : 'Ginette', 'lastname' : 'Beurette', 'email' : 'GB@gmel.com'}"; 
        Gson gson2 = new Gson(); //optionnel
    	User userObject = gson2.fromJson(jsonString2, User.class);
    	System.out.println(userObject);
    	 
    	System.out.println("-------------------");
    	Employe employe = new Employe();
    	employe.setId(1);
    	employe.setFirstName("Bruce");
    	employe.setLastName("Wayne");
    	employe.setRoles(Arrays.asList("Admin","Manager"));
    	Gson gson3 = new Gson(); //optionnel
    	System.out.println(gson3.toJson(employe));
    	
    	System.out.println("-------------------");
    	Gson gson4 = new Gson(); //optionnel
    	System.out.println(gson4.fromJson("{'id':1,'firstName':'Bubu','lastName':'WaWa','roles':['Mecano','Magicien']}", Employe.class));
    	
    	System.out.println("-------------------");
    	Carre monCarre = new Carre(3.2);
    	System.out.println(monCarre);
    	
    	System.out.println("---------Serialisation personnalisée----------");
    	GsonBuilder gsonBuilder = new GsonBuilder(); 
    	gsonBuilder.registerTypeAdapter(Date.class, new DateSerializer()); 
    	gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
    	Gson gson5 = gsonBuilder.create(); 
    	
    	employe.setBirthDate(new Date());
    	System.out.println(gson5.toJson(employe));
    	
    	System.out.println(gson5.fromJson("{'id':1,'firstName':'Clark','lastName':'Kent','roles':['ADMIN','MANAGER'],'birthDate':'12/08/1998'}", Employe.class)); 
    	
    }
}
