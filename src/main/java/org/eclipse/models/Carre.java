package org.eclipse.models;

public class Carre {
	
	private double longueur;
	private double surface;
	
	public Carre(double d) {
		super();
		this.longueur = d;
		this.surface = d * d;
	}

	public Carre() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Carre [longueur=" + longueur + ", surface=" + surface + "]";
	}
	
}
